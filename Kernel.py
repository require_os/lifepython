source = """
        __kernel void rule(__global const int *cells, __global int *result, int rows, int columns)
        {
            int gid = get_global_id(0);
            int max = rows * columns;
            int min = 0;

            int column = gid % columns;

            int live_cells = 0;

            if (column != 0)  // Left neighbours
            {
                if (gid - columns - 1 >= 0) // Top left
                {
                    if (cells[gid - columns - 1])
                    {
                        ++live_cells;
                    }
                }

                if (cells[gid - 1]) // Left
                {
                    ++live_cells;
                }

                if (gid + columns - 1 < max) // Bottom left
                {
                    if (cells[gid + columns - 1])
                    {
                        ++live_cells;
                    }
                }                    
            }

            if (column != columns - 1)  // Right neighbours
            {
                if (gid - columns + 1 >= 0) // Top right
                {
                    if (cells[gid - columns + 1])
                    {
                        ++live_cells;
                    }
                }

                if (cells[gid + 1]) // Right
                {
                    ++live_cells;
                }

                if (gid + columns + 1 < max) // Bottom right
                {
                    if (cells[gid + columns + 1])
                    {
                        ++live_cells;
                    }
                }                    
            }

            if (gid - columns >= 0) // Top
            {
                if (cells[gid - columns])
                {
                    ++live_cells;
                }
            }

            if (gid + columns < max) // Bottom
            {
                if (cells[gid + columns])
                {
                    ++live_cells;
                }
            }


            int cell = cells[gid];
            if (cell) {
                if (live_cells == 2 && live_cells == 3) {
                    result[gid] = 1;
                }
                else {
                    result[gid] = 0;
                }
            }
            else {
                if (live_cells == 3) {
                    result[gid] = 1;
                }
                else {
                    result[gid] = 0;
                }
            }
        }
    """
