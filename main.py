# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 15:20:21 2018

@author: b.ulzutuev
"""

from App import App

if __name__ == "__main__":
    theApp = App()

    theApp.on_execute()
