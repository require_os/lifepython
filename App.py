# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 17:02:49 2018

@author: b.ulzutuev
"""

import pygame
import numpy as np
import pyopencl as cl
"""from pyopencl.tools import get_gl_sharing_context_properties
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from OpenGL.arrays import vbo"""
import sys
from pygame.locals import *
import Kernel

class App:
    def __init__(self):
        self._running = True
        self._display_surf = None
        self.size = self.width, self.height = 800, 600
        self.aspect = self.width / self.height
        self.rows = 800
        self.columns = 600

        self.rowlen = self.width / self.rows
        self.collen = self.width / self.columns

        self.deltatime = 1

    def on_init(self):
        pygame.init()
        self._display_surf = pygame.display.set_mode(self.size, SWSURFACE)
        self.cells = np.random.random_integers(0, 1, self.rows * self.columns).astype(np.int32)
        
        print("Python " + sys.version)

        self.platforms = cl.get_platforms()

        print("Accesible platforms:")
        for p in self.platforms:
            print(p.name + "|" + p.vendor + "|" + p.version + "|" + p.profile)

        self.gpu_devices = []
        for p in self.platforms:
            for d in p.get_devices(cl.device_type.GPU):
                self.gpu_devices.append(d)

        print("Accesible GPU devices:")
        for d in self.gpu_devices:
            print(d.name + "|" + d.version + "|" + d.vendor)

        self.contexts = []
        for d in self.gpu_devices:
            self.contexts.append(cl.Context([d]))

        self.program = cl.Program(self.contexts[0], Kernel.source).build()

        self.queues = []
        for c in self.contexts:
            self.queues.append(cl.CommandQueue(c))

        self.currentCellsBuffer = cl.Buffer(
            self.contexts[0],
            cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR,
            hostbuf=self.cells
        )
        cl.enqueue_copy(self.queues[0], self.currentCellsBuffer, self.cells)

        self.nextCellsBuffer = cl.Buffer(
            self.contexts[0],
            cl.mem_flags.WRITE_ONLY,
            size=self.cells.nbytes
        )


    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False

    def on_loop(self):
        kernel = self.program.rule
        kernel.set_scalar_arg_dtypes([None, None, np.int32, np.int32])
        kernel.set_args(self.currentCellsBuffer, self.nextCellsBuffer, self.rows, self.columns)
        cl.enqueue_nd_range_kernel(self.queues[0], kernel, self.cells.shape, None)

        cl.enqueue_copy(self.queues[0], self.cells, self.nextCellsBuffer)

        cl.enqueue_copy(self.queues[0], self.currentCellsBuffer, self.nextCellsBuffer)
        pygame.time.wait(self.deltatime)

    def on_render(self):  # REWRITE IN OPENGL !!?
        for i in range(self.rows):
            for j in range(self.columns):
                if self.cells[i * self.columns + j] > 0:
                    pygame.draw.rect(self._display_surf, (255, 255, 255),
                                     pygame.Rect(j * self.collen, i * self.rowlen, self.collen, self.rowlen))
                else:
                    pygame.draw.rect(self._display_surf, (0, 0, 0),
                                     pygame.Rect(j * self.collen, i * self.rowlen, self.collen, self.rowlen))

        pygame.display.flip()

    def on_cleanup(self):
        pygame.quit()

    def on_execute(self):
        if self.on_init() is False:
            self._running = False

        while(self._running):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()
